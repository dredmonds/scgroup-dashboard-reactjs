// ajax and loading actions
export const BEGIN_AJAX_CALL = 'BEGIN_AJAX_CALL';
export const AJAX_CALL_ERROR = 'AJAX_CALL_ERROR';

// Pin actions
export const RETRIEVED_SERVER_URL = 'RETRIEVED_SERVER_URL';
export const RETRIEVED_CLIENT_CONFIG = 'RETRIEVED_CLIENT_CONFIG';

// User actions
export const USER_LOGGED_IN_SUCCESS = 'USER_LOGGED_IN_SUCCESS';
