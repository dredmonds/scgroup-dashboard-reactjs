// src/actions/pin.js
import {ajaxCallError, beginAjaxCall} from './ajaxStatusActions';
import {push} from 'react-router-redux';
import * as types from './actionTypes';

export function getServerURL(orgId) {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    return fetch('https://admin.staffconnectapp.com/api/'+orgId)
      .then((response) => response.text())
      .then((responseJson) => {
        dispatch(serverURLRetrievedSuccess(responseJson));
        return responseJson;
      })
      .catch((error) => {
        console.error(error);
        dispatch(ajaxCallError(error));
        // @TODO better error handling
        throw(error);
      });
  };
}

export function getClientConfig(serverURL, orgId) {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    return fetch(serverURL+'config/'+orgId)
      .then((response) => response.json())
      .then((responseJson) => {
        // dispatch(push('/pages/login'));
        window.location.href = '#/pages/login';
        dispatch(clientConfigRetrievedSuccess(responseJson, orgId));
        return responseJson;
      })
      .catch((error) => {
        console.error(error);
        dispatch(ajaxCallError(error));
        // @TODO better error handling
        throw(error);
      });
  };
}

export function serverURLRetrievedSuccess(url) {
  return {
    type: types.RETRIEVED_SERVER_URL,
    serverURL:url
  };
}

export function clientConfigRetrievedSuccess(clientConfig, orgId) {
  return {
    type: types.RETRIEVED_CLIENT_CONFIG,
    clientConfig:clientConfig,
    orgId:orgId
  };
}
