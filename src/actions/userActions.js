// src/actions/pin.js
import {ajaxCallError, beginAjaxCall} from './ajaxStatusActions';
import {push} from 'react-router-redux';
import * as types from './actionTypes';

export function loginUserSuccess(token) {
  return (dispatch) => {
    dispatch(userLoggedInSuccess(JSON.parse(token)));
  };
}

export function postLogin(userForm) {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    var options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'code':localStorage.orgId
      },
      body: JSON.stringify(userForm)
    }
    return fetch(localStorage.serverURL+'auth/login', options)
      .then((response) => response.json())
      .then((responseJson) => {
        window.location.href = '#/';
        dispatch(userLoggedInSuccess(responseJson));
        return responseJson;
      })
      .catch((error) => {
        dispatch(ajaxCallError(error));
        // @TODO better error handling
        throw(error);
      });
  };
}

export function getUsers() {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    var options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'code':localStorage.orgId,
        'token':JSON.parse(localStorage.user).token
      }
    }
    return fetch(localStorage.serverURL+'admin/users', options)
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson;
      })
      .catch((error) => {
        dispatch(ajaxCallError(error));
        // @TODO better error handling
        throw(error);
      });
  };
}

export function getUser(id) {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    var options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'code':localStorage.orgId,
        'token':JSON.parse(localStorage.user).token
      }
    }
    return fetch(localStorage.serverURL+'directory?user='+id, options)
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson;
      })
      .catch((error) => {
        dispatch(ajaxCallError(error));
        // @TODO better error handling
        throw(error);
      });
  };
}

export function updateUser(data) {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    var options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'code':localStorage.orgId,
        'token':JSON.parse(localStorage.user).token
      },
      body: JSON.stringify(data)
    }
    return fetch(localStorage.serverURL+'admin/users', options)
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson;
      })
      .catch((error) => {
        dispatch(ajaxCallError(error));
        // @TODO better error handling
        throw(error);
      });
  };
}

export function addUser(data) {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    var options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'code':localStorage.orgId,
        'token':JSON.parse(localStorage.user).token
      },
      body: JSON.stringify(data)
    }
    return fetch(localStorage.serverURL+'admin/users', options)
      .then((response) => response.text())
      .then((responseJson) => {
        if(responseJson != 'Success') {
          throw(responseJson)
        }
        return responseJson;
      })
      .catch((error) => {
        dispatch(ajaxCallError(error));
        // @TODO better error handling
        throw(error);
      });
  };
}


export function getLocations() {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    var options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'code':localStorage.orgId,
        'token':JSON.parse(localStorage.user).token
      }
    }
    return fetch(localStorage.serverURL+'auth/locations', options)
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson;
      })
      .catch((error) => {
        dispatch(ajaxCallError(error));
        // @TODO better error handling
        throw(error);
      });
  };
}

export function getDepartments() {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    var options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'code':localStorage.orgId,
        'token':JSON.parse(localStorage.user).token
      }
    }
    return fetch(localStorage.serverURL+'auth/departments', options)
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson;
      })
      .catch((error) => {
        dispatch(ajaxCallError(error));
        // @TODO better error handling
        throw(error);
      });
  };
}


export function userLoggedInSuccess(user) {
  return {
    type: types.USER_LOGGED_IN_SUCCESS,
    user:user
  };
}
