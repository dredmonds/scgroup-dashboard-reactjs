import React from 'react';
import {connect} from 'react-redux';

export function requireAuthentication(Component) {

    class AuthenticatedComponent extends React.Component {

        componentWillMount() {
            this.checkAuth();
        }

        componentWillReceiveProps(nextProps) {
            this.checkAuth();
        }

        checkAuth() {
            if (!this.props.isAuthenticated) {
                let redirectAfterLogin = this.props.location.pathname;
                // this.props.dispatch(pushState(null, `/pages/login?next=${redirectAfterLogin}`));
                window.location.href = `#/pages/pin?next=${redirectAfterLogin}`;
            }
        }

        render() {
            return (
                <div>
                    {this.props.isAuthenticated === true
                        ? <Component {...this.props}/>
                        : null
                    }
                </div>
            )
        }

    }

    const mapStateToProps = (state) => ({
        isAuthenticated: state.user.token ? true : false
    });

    return connect(mapStateToProps)(AuthenticatedComponent);

}
