import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        StaffConnect Group Limited &copy; 2017
        <span className="float-right">Powered by <a href="https://StaffConnectApp.com">StaffConnect App</a></span>
      </footer>
    )
  }
}

export default Footer;
