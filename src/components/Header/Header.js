import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dropdown, DropdownMenu, DropdownItem } from 'reactstrap';

class Header extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.logout = this.logout.bind(this);
    this.linkTo = this.linkTo.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle(e) {
    if(e) e.preventDefault();
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-compact');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('aside-menu-hidden');
  }

  logout () {
    this.toggle()
    localStorage.clear();
    window.location.href = '#/pages/pin';
  }

  linkTo (link) {
    window.location.href = link;
  }

  render() {
    return (
      <header className="app-header navbar">
        <button className="navbar-toggler mobile-sidebar-toggler d-lg-none" onClick={this.mobileSidebarToggle} type="button">&#9776;</button>
        <a className="navbar-brand" href="#"></a>
        <ul className="nav navbar-nav d-md-down-none">
          <li className="nav-item">
            <a className="nav-link navbar-toggler sidebar-toggler" onClick={this.sidebarToggle} href="#">&#9776;</a>
          </li>
          <li className="nav-item px-3">
            <a className="nav-link" href="#">Dashboard</a>
          </li>
          <li className="nav-item px-3">
            <a className="nav-link" href="#/users">Users</a>
          </li>
          <li className="nav-item px-3">
            <a className="nav-link" href="#">Settings</a>
          </li>
        </ul>
        <ul className="nav navbar-nav ml-auto">
          <li className="nav-item">
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <a onClick={this.toggle} className="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="" role="button" aria-haspopup="true" aria-expanded={this.state.dropdownOpen}>
                <img src={this.props.user.avatar} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                <span className="d-md-down-none">{this.props.user.first_name} {this.props.user.last_name}</span>
              </a>

              <DropdownMenu className="dropdown-menu-right">

                <DropdownItem onClick={this.linkTo.bind(this, '#/users/'+this.props.user._id)}><i className="fa fa-user"></i> Profile</DropdownItem>
                <DropdownItem onClick={this.logout}><i className="fa fa-lock"></i> Logout</DropdownItem>

              </DropdownMenu>
            </Dropdown>
          </li>
          <li className="nav-item d-md-down-none">
            <a className="nav-link navbar-toggler aside-menu-toggler hide" onClick={this.asideToggle} href="#"></a>
          </li>
        </ul>
      </header>
    )
  }
}


function mapStateToProps(state, props) {
    return {
        user: state.user
    };
}
export default connect(mapStateToProps)(Header);
