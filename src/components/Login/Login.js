import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as loginActions from '../../actions/userActions';

class Login extends Component {
  constructor(props) {
    super(props);

    this.updateUserState = this.updateUserState.bind(this);
    this.onSubmitLogin = this.onSubmitLogin.bind(this);

    this.state = {
      userForm:{},
      submitting:false
    }
  }

  updateUserState(event) {
    const field = event.target.name;
    let userForm = Object.assign({}, this.state.userForm);
    userForm[field] = event.target.value;
    return this.setState({userForm: userForm});
  }

  onSubmitLogin (event) {
    if(event) event.preventDefault();
    this.setState({submitting:true});
    this.props.actions.postLogin(this.state.userForm)
      .then( (success) => {
        console.log(success)
      })
      .catch( (err) => {
        this.setState({submitting:false});
      })
  }

  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-6">
            <div className="card-group mb-0">
              <div className="card p-4">
                <form className="card-block">
                  <h1>Login</h1>
                  <p className="text-muted">Sign In to your account</p>
                  <div className="input-group mb-3">
                    <span className="input-group-addon"><i className="icon-user"></i></span>
                    <input name="username" type="text" className="form-control" placeholder="Username" onChange={this.updateUserState} value={this.state.userForm.username}/>
                  </div>
                  <div className="input-group mb-4">
                    <span className="input-group-addon"><i className="icon-lock"></i></span>
                    <input name="password" type="password" className="form-control" placeholder="Password" onChange={this.updateUserState} value={this.state.userForm.password}/>
                  </div>
                  <div className="row">
                    <div className="col-6">
                      <button type="submit" className="btn btn-primary px-4" onClick={this.onSubmitLogin} disabled={this.state.submitting}>{this.state.submitting ? 'Submitting...':'Next'}</button>
                    </div>
                    <div className="col-6 text-right">
                      <button type="button" className="btn btn-link px-0">Forgot password?</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
    return {
        user: state.user
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(loginActions, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);
