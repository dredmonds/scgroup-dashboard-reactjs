import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as configActions from '../../actions/config';

class Pin extends Component {
  constructor(props) {
    super(props);

    this.updatePinState = this.updatePinState.bind(this);
    this.onSubmitPin = this.onSubmitPin.bind(this);
    this.getClientConfig = this.getClientConfig.bind(this);

    this.state = {
      pin:null,
      submitting:false
    }
  }

  updatePinState(event) {
    this.setState({pin: event.target.value});
  }

  onSubmitPin (event) {
    if(event) event.preventDefault();
    this.setState({submitting:true});
    this.props.actions.getServerURL(this.state.pin)
      .then( (success) => {
        this.getClientConfig(this.props.config.serverURL);
      })
      .catch( (err) => {
        this.setState({submitting:false});
      })

  }

  getClientConfig (serverURL) {
    this.props.actions.getClientConfig(serverURL, this.state.pin)
      .then( (success) => {
        this.setState({submitting:false});
      })
      .catch( (err) => {
        this.setState({submitting:false});
      })
  }

  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-6">
            <div className="card-group mb-0">
              <div className="card p-4">
                <form className="card-block">
                  <h1>Organisation ID</h1>
                  <p className="text-muted">Please enter your Organisation ID</p>
                  <div className="input-group mb-3">
                    <span className="input-group-addon"><i className="icon-globe"></i></span>
                    <input type="text" className="form-control" placeholder="Organisation ID" onChange={this.updatePinState} value={this.state.pin}/>
                  </div>
                  <div className="row">
                    <div className="col-6">
                      <button type="submit" className="btn btn-primary px-4" onClick={this.onSubmitPin} disabled={this.state.submitting}>{this.state.submitting ? 'Submitting...':'Next'}</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
    return {
        config: state.config
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(configActions, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Pin);
