import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as loginActions from '../../actions/userActions';

class UserDetailView extends Component {

  constructor(props) {
    super(props);

    this.updateUserState = this.updateUserState.bind(this);
    this.getUser = this.getUser.bind(this);
    this.updateUser = this.updateUser.bind(this);
    this.getLocations = this.getLocations.bind(this);
    this.getDepartments = this.getDepartments.bind(this);

    this.state = {
      user:{},
      loading:false,
      successMessage:null
    }

    this.getUser();
    this.getLocations();
    this.getDepartments();
  }

  updateUserState(event) {
    const field = event.target.name;
    let userForm = Object.assign({}, this.state.user);
    userForm[field] = event.target.value;
    return this.setState({user: userForm});
  }

  getUser() {
    this.setState({loading:true});
    this.props.actions.getUser(this.props.params.id)
      .then( (success) => {
        this.setState({ user: success[0] });
      })
      .catch( (err) => {
        this.setState({loading:false});
      })
  }

  getLocations() {
    this.setState({loading:true});
    this.props.actions.getLocations()
      .then( (success) => {
        this.setState({ locations: success });
      })
      .catch( (err) => {
        this.setState({loading:false});
      })
  }

  getDepartments() {
    this.setState({loading:true});
    this.props.actions.getDepartments()
      .then( (success) => {
        this.setState({ departments: success });
      })
      .catch( (err) => {
        this.setState({loading:false});
      })
  }

  updateUser() {
    this.setState({saving:true, successMessage:null});

    var updateData = {
      _id: this.state.user._id,
      username: this.state.user.username,
      first_name: this.state.user.first_name,
      last_name: this.state.user.last_name,
      department: this.state.user.department._id || this.state.user.department,
      location: this.state.user.location._id || this.state.user.location,
      role: this.state.user.role,
      disabled: this.state.user.disabled
    }
    this.props.actions.updateUser(updateData)
      .then( (success) => {
        this.setState({saving:false, successMessage:'User successfully updated.'});
      })
      .catch( (err) => {
        this.setState({saving:false, errorMessage:err});
      })
  }

  render() {
    return (
      <div className="animated fadeIn">
        <div className="row">
          <div className="col-sm-12">
            <div className="card">
              <div className="card-header">
                <strong>Edit User</strong>
              </div>
              <div className="card-block">
                <form action="" method="post" encType="multipart/form-data" className="form-2orizontal ">
                  <div className="form-group row">
                    <label className="col-md-3 form-control-label">Email</label>
                    <div className="col-md-9">
                      { this.state.user.username }
                    </div>
                  </div>
                  <div className={this.state.user.first_name ? 'form-group row':'form-group row has-danger has-feedback'}>
                    <label className="col-md-3 form-control-label" htmlFor="text-input">First name</label>
                    <div className="col-md-9">
                      <input type="text" id="text-input" name="first_name" className={this.state.user.first_name ? 'form-control':'form-control form-control-danger'} onChange={this.updateUserState} value={ this.state.user.first_name } placeholder="Please enter the user's first name"/>
                    </div>
                  </div>
                  <div className={this.state.user.last_name ? 'form-group row':'form-group row has-danger has-feedback'}>
                    <label className="col-md-3 form-control-label" htmlFor="text-input">Last name</label>
                    <div className="col-md-9">
                      <input type="text" id="text-input" name="last_name" className={this.state.user.last_name ? 'form-control':'form-control form-control-danger'} onChange={this.updateUserState} value={ this.state.user.last_name } placeholder="Please enter the user's last name"/>
                    </div>
                  </div>

                  <div className={this.state.user.location ? 'form-group row':'form-group row has-danger has-feedback'}>
                    <label className="col-md-3 form-control-label" htmlFor="select">Location</label>
                    <div className="col-md-9">
                      <select name="location" className="form-control" onChange={this.updateUserState} value={this.state.user.location ? this.state.user.location._id : ''}>
                        <option value="">Please select users location</option>
                        {
                         this.state.locations ? this.state.locations.map((location) => (<option key={location._id} value={location._id}>{location.name}</option>) ) : ''
                        }
                      </select>
                    </div>
                  </div>

                  <div className={this.state.user.department ? 'form-group row':'form-group row has-danger has-feedback'}>
                    <label className="col-md-3 form-control-label" htmlFor="select">Department</label>
                    <div className="col-md-9">
                      <select name="department" className="form-control" onChange={this.updateUserState} value={this.state.user.department ? this.state.user.department._id : ''}>
                        <option value="">Please select users department</option>
                        {
                         this.state.departments ? this.state.departments.map((department) => (<option key={department._id} value={department._id}>{department.name}</option>) ) : ''
                        }
                      </select>
                    </div>
                  </div>


                  <div className={this.state.user.role ? 'form-group row':'form-group row has-danger has-feedback'}>
                    <label className="col-md-3 form-control-label">Access</label>
                    <div className="col-md-9">
                      <label className="radio-inline">
                        <input type="radio" name="role" onChange={this.updateUserState} value="user" checked={this.state.user.role === 'user'}  /> User &nbsp;&nbsp;
                      </label>
                      <label className="radio-inline">
                        <input type="radio" name="role" onChange={this.updateUserState} value="admin" checked={this.state.user.role === 'admin'}/> Admin
                      </label>
                    </div>
                  </div>

                  <div className={this.state.user.disabled ? 'form-group row':'form-group row has-danger has-feedback'}>
                    <label className="col-md-3 form-control-label">Disabled</label>
                    <div className="col-md-9">
                      <label className="radio-inline">
                        <input type="radio" name="disabled" onChange={this.updateUserState} value="true" checked={this.state.user.disabled === 'true'}  /> Yes &nbsp;&nbsp;
                      </label>
                      <label className="radio-inline">
                        <input type="radio" name="disabled" onChange={this.updateUserState} value="false" checked={this.state.user.disabled === 'false'}/> No
                      </label>
                    </div>
                  </div>

                  <div className="form-group row">
                    <label className="col-md-3 form-control-label">Verified</label>
                    <div className="col-md-9">
                      { this.state.user.pin ? 'No': 'Yes' }
                    </div>
                  </div>

                  <div className="form-group row">
                    <label className="col-md-3 form-control-label">Last login</label>
                    <div className="col-md-9">
                      { this.state.user.lastLogin }
                    </div>
                  </div>

                  <div className="form-group row">
                    <label className="col-md-3 form-control-label">Last active</label>
                    <div className="col-md-9">
                      { this.state.user.lastActive }
                    </div>
                  </div>

                  { this.state.errorMessage ? (<div className="alert alert-danger" role="alert">{this.state.errorMessage}</div>) : '' }
                  { this.state.successMessage ? (<div className="alert alert-success" role="alert">{this.state.successMessage}</div>) : '' }

                </form>
              </div>
              <div className="card-footer">
                <button
                  type="submit"
                  disabled={
                    !this.state.user.first_name
                    || !this.state.user.last_name
                    || !this.state.user.location
                    || !this.state.user.department
                    || !this.state.user.role
                    || this.state.saving
                  }
                  className="btn btn-md btn-primary"
                  onClick={this.updateUser}
                  >
                    { this.state.saving ? 'Saving' : 'Save changes' }
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state, props) {
    return {
        user: state.user
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(loginActions, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserDetailView);
