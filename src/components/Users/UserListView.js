import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SearchInput, {createFilter} from 'react-search-input';
import * as loginActions from '../../actions/userActions';
import {Table, Tr, Td, Thead, Th} from '../reactable';

const KEYS_TO_FILTERS = ['first_name', 'last_name', 'username', 'location.name', 'department.name']

class Widgets extends Component {

  constructor(props) {
    super(props);

    this.searchUpdated = this.searchUpdated.bind(this);
    this.getUsers = this.getUsers.bind(this);

    this.state = {
      users:[],
      loading:false,
      searchTerm:''
    }

    this.getUsers();
  }

  searchUpdated(event){
    this.setState({searchTerm: event.target.value});
  }

  getUsers () {
    this.setState({loading:true});
    this.props.actions.getUsers()
      .then( (success) => {
        this.setState({users: success});
      })
      .catch( (err) => {
        this.setState({loading:false});
      })
  }

  userDetailView (user) {
    window.location.href = '#/users/'+user._id;
  }

  render() {
    const filteredUsers = this.state.users.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

    return (
      <div className="animated fadeIn">
        <div className="btn-group" role="group" aria-label="Button group with nested dropdown">
          <a href="#/users/add"><button type="button" className="btn btn-primary">Add user</button></a>&nbsp;
          <button type="button" className="btn btn-primary hide">Export to CSV</button>
        </div>
        <br/><br/>
        <div className="row">
          <div className="col-sm-12">
            <div className="input-group">
              <input type="text" className="form-control" placeholder="Search users" aria-label="Search users" onChange={this.searchUpdated} />
            </div>
          </div>
        </div>
        <br/>
        <div className="">
          <Table className="table table-hover mb-0 hidden-sm-down table-responsive" sortable={true} itemsPerPage={25}>
            <Thead className="thead-default">
                <Th column="Avatar" className="text-center col-sm-1"><i className="icon-people"></i></Th>
                <Th column="Name" className="col-sm-3">Name</Th>
                <Th column="Location" className="col-sm-2">Location</Th>
                <Th column="Department" className="col-sm-2">Department</Th>
                <Th column="Role" className="col-sm-1">Role</Th>
                <Th column="Activity" className="col-sm-3">Activity</Th>
            </Thead>
              {
               filteredUsers.map((user) => (
                 <Tr key={user._id} onClick={this.userDetailView.bind(this, user)}>
                   <Td column="Avatar" className="text-center">
                     <div className="avatar">
                       <img src={user.avatar ? user.avatar : 'https://staging.staffconnectapp.com/img/profile-dummy.png'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                       <span className={'avatar-status '+(user.pin ? 'badge-warning':'badge-success')}></span>
                     </div>
                   </Td>
                   <Td column="Name" value={user.first_name}>
                    <div>
                       <div>{ user.first_name } { user.last_name }</div>
                       <div className="small text-muted">
                         <span>{user.pin ? 'Unverified':'Verified'}</span> | Registered: { user.created_at }
                       </div>
                    </div>
                   </Td>
                   <Td column="Location" className="">
                     { user.location ? user.location.name : 'Not set' }
                   </Td>
                   <Td column="Department">
                     { user.department ? user.department.name : 'Not set' }
                   </Td>
                   <Td  column="Role"className="">
                     { user.role }
                   </Td>
                   <Td column="Activity" value={user.lastLogin}>
                    <div>
                     <div className="small text-muted">Last login</div>
                     <strong>{user.lastLogin}</strong>
                    </div>
                   </Td>
                 </Tr>
               ))
              }
          </Table>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state, props) {
    return {
        user: state.user
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(loginActions, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Widgets);
