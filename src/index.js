import React from 'react';
import ReactDOM from 'react-dom';
import {syncHistoryWithStore} from 'react-router-redux';
import { Router, browserHistory, hashHistory } from 'react-router';
import routes from './routes';
import { Provider } from 'react-redux';
import {loginUserSuccess} from './actions/userActions';

// Store
import Store from './store';
import initialState from './reducers/initialState';

const StoreInstance = Store(initialState);
const history = syncHistoryWithStore(browserHistory, StoreInstance);

let token = localStorage.getItem('user');
if (token !== null) {
    StoreInstance.dispatch(loginUserSuccess(token));
}

ReactDOM.render(
  <Provider store={StoreInstance}>
    <Router routes={routes(StoreInstance)} history={hashHistory} />
  </Provider>,
  document.getElementById('root')
);
