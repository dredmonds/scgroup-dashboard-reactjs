import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function configReducer(state = initialState.config, action) {
  switch (action.type) {
    case types.RETRIEVED_SERVER_URL:
      localStorage['serverURL'] = action.serverURL;
      return Object.assign({}, state, {serverURL:action.serverURL});
    case types.RETRIEVED_CLIENT_CONFIG:
      localStorage['orgId'] = action.orgId;
      return Object.assign({}, state, {clientConfig:JSON.parse(action.clientConfig.legacy), orgId:action.orgId});
    default:
      return state;
  }
}
