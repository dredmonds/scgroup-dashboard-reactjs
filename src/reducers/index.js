// src/reducers/index.js
import { combineReducers } from 'redux';
import config from './config';
import user from './userReducer';
import ajaxCallsInProgress from './ajaxStatusReducer';
import { routerReducer } from 'react-router-redux';

const rootReducer = combineReducers({
    routing: routerReducer,
    config,
    user,
    ajaxCallsInProgress
});
export default rootReducer;
