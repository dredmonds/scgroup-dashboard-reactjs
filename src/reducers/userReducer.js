import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function userReducer(state = initialState.user, action) {
  switch (action.type) {
    case types.USER_LOGGED_IN_SUCCESS:
      if(action.user.user) {
        action.user.user.token = action.user.token;
        localStorage.user = JSON.stringify(action.user.user);
      } else {
        localStorage.user = JSON.stringify(action.user);
      }
      return Object.assign({}, state, action.user.user ? action.user.user : action.user);
    default:
      return state;
  }
}
