import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import {requireAuthentication} from './components/AuthenticatedComponent';

// Containers
import Full from './containers/Full/'
import Simple from './containers/Simple/'

import Charts from './views/Charts/'
import Dashboard from './views/Dashboard/'
import Buttons from './views/Components/Buttons/'
import Cards from './views/Components/Cards/'
import Forms from './views/Components/Forms/'
import Modals from './views/Components/Modals/'
import SocialButtons from './views/Components/SocialButtons/'
import Switches from './views/Components/Switches/'
import Tables from './views/Components/Tables/'
import Tabs from './views/Components/Tabs/'
import FontAwesome from './views/Icons/FontAwesome/'
import SimpleLineIcons from './views/Icons/SimpleLineIcons/'
import Pin from './components/Pin/'
import Login from './components/Login/'
import Register from './views/Pages/Register/'
import Page404 from './views/Pages/Page404/'
import Page500 from './views/Pages/Page500/'
import Users from './components/Users/UserListView'
import UserDetailView from './components/Users/UserDetailView'
import UserAddView from './components/Users/UserAddView'

export default function Routes(store) {

  return (
    <Router history={store}>
      <Route path="/" name="Home" component={requireAuthentication(Full)}>
        <IndexRoute component={Dashboard}/>
        <Route path="dashboard" name="Dashboard" component={Dashboard}/>
        <Route path="components/" name="Components">
          <IndexRoute component={Buttons}/>
          <Route path="buttons" name="Buttons" component={Buttons}/>
          <Route path="cards" name="Cards" component={Cards}/>
          <Route path="forms" name="Forms" component={Forms}/>
          <Route path="modals" name="Modals" component={Modals}/>
          <Route path="social-buttons" name="Social Buttons" component={SocialButtons}/>
          <Route path="switches" name="Swithces" component={Switches}/>
          <Route path="tables" name="Tables" component={Tables}/>
          <Route path="tabs" name="Tabs" component={Tabs}/>
        </Route>
        <Route path="icons/" name="Icons">
          <IndexRoute component={FontAwesome}/>
          <Route path="font-awesome" name="Font Awesome" component={FontAwesome}/>
          <Route path="simple-line-icons" name="Simple Line Icons" component={SimpleLineIcons}/>
        </Route>
        <Route path="users" name="Users">
          <IndexRoute component={Users}/>
          <Route path="add" name="Add user" component={UserAddView}/>
          <Route path=":id" name="User" component={UserDetailView}/>
        </Route>
        <Route path="charts" name="Charts" component={Charts}/>
      </Route>
      <Route path="pages/" name="Pages" component={Simple}>
        <IndexRoute component={Page404}/>
        <Route path="pin" name="Pin Page" component={Pin}/>
        <Route path="login" name="Login Page" component={Login}/>
        <Route path="register" name="Register Page" component={Register}/>
        <Route path="404" name="Page 404" component={Page404}/>
        <Route path="500" name="Page 500" component={Page500}/>
      </Route>
    </Router>
  );
}
